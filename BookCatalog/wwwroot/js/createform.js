﻿const sub = document.querySelector("input[type=submit]");
const bookApi = "/api/Book";

sub.addEventListener("click", (e) =>{
  e.returnValue = false;
  if($("form").valid()) {
    const form = new FormData(document.querySelector("form"));
    fetch(bookApi, {
      method: "POST",
      body: form
    }).then(resp => {
      if (resp.ok) {
        location.href = "/";
      }
    });
  }
});
