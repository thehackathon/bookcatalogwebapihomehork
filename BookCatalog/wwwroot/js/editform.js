const sub = document.querySelector("input[type=submit]");
const bookApi = "/api/Book";

sub.addEventListener("click", (e) =>{
  e.returnValue = false;
  if($("form").valid()){
    const form = new FormData(document.querySelector("form"));
    const id = document.querySelector("input#bookId").value;
    fetch(`${bookApi}/${id}`, {
      method: "PUT",
      body: form
    }).then(resp => {
      if(resp.ok){
        location.href = "/";
      }
    });
  }
});
