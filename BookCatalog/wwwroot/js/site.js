﻿const genresElem = document.querySelector("#genres");
const booksTable = document.querySelector("#books");
const booksElem = document.querySelector("#books tbody");
const title = document.querySelector(".title > span");
const genreApi = "/api/Genre";
const bookApi = "/api/Book";
const selectedGenreClass = "selected";

async function deleteBook(){
  const tr = this.closest("tr");
  const bookId = tr.getAttribute("data-id");
  await fetch(`${bookApi}/${bookId}`, {
    method: 'DELETE'
  }).then(resp => {
    if(resp.status === 200){
      tr.remove();
    }
  });
}

async function editBook(){
  const tr = this.closest("tr");
  const bookId = tr.getAttribute("data-id");
  location.href = `/Home/EditBook/${bookId}`;
}

async function getBooks(genreId) {
  await fetch(bookApi+`?genreIdStr=${genreId}`)
    .then(resp => resp.json())
    .then(json => {
      booksTable.classList.remove("hidden");
      while(booksElem.firstChild){
        booksElem.removeChild(booksElem.firstChild);
      }
      for (const book of json) {
        let tr = document.createElement("tr");
        tr.setAttribute("data-id", book.id);
        tr.classList.add("book");
        tr.innerHTML =
`<td><a href="/api/Book/${book.id}">${book.title}</a></td>
<td>${book.author}</td>
<td>${book.publisher}</td>
<td>${new Date(book.publishDate).toDateString()}</td>
<td class="buttons">
    <button class="btn edit">Edit</button>
    <button class="btn delete">Delete</button>
</td>`;
        const deleteBtn = tr.querySelector(".delete");
        deleteBtn.addEventListener("click", deleteBook);

        const editBtn = tr.querySelector(".edit");
        editBtn.addEventListener("click", editBook);
        booksElem.appendChild(tr);
      }
    });
}

function openGenre(){
  const genreId = this.getAttribute("data-id");
  const genreName = this.querySelector("label").textContent;
  title.textContent = `Books - ${genreName}`;

  const selected = genresElem.querySelector(`.${selectedGenreClass}`);
  if(selected){
    selected.classList.remove(selectedGenreClass);
  }
  this.classList.add(selectedGenreClass);
  getBooks(genreId);
}

async function getGenres() {
  await fetch(genreApi)
    .then(resp => resp.json())
    .then(json => {
      for (const genre of json) {
        let div = document.createElement("div");
        div.setAttribute("data-id", genre.id);
        div.classList.add("item");
        div.innerHTML = `<label>${genre.name}</label>`;
        div.addEventListener("click", openGenre);
        genresElem.appendChild(div);
      }
    });
}

getGenres();
