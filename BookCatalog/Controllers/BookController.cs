﻿namespace BookCatalog.Controllers
{
    using Microsoft.AspNetCore.Mvc;
    using System.Collections.Generic;
    using Models;
    using Models.Repository;

    [Route("api/Book")]
    [ApiController]
    public class BookController : ControllerBase
    {
        private readonly IBookRepository _repository;
        public BookController([FromServices] IBookRepository repository)
        {
            this._repository = repository;
        }

        // GET: api/Book?genreIdStr=1
        [HttpGet]
        public IEnumerable<Book> Get([FromQuery] string genreIdStr)
        {
            if (!string.IsNullOrWhiteSpace(genreIdStr) && int.TryParse(genreIdStr, out int genreId))
            {
                return _repository.GetByGenre(genreId);
            }

            return _repository.Get();
        }

        // GET: api/Book/5
        [HttpGet("{id}", Name = "GetBookById")]
        public Book Get(int id)
        {
            return _repository.Get(id);
        }

        // POST: api/Book
        [HttpPost]
        public IActionResult Post([FromForm] Book value)
        {
            if (ModelState.IsValid)
            {
                return Ok(_repository.Add(value));
            }
            return ValidationProblem();
        }

        // PUT: api/Book/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromForm] Book value)
        {
            if (ModelState.IsValid)
            {
                _repository.Update(id, value);
                return Ok();
            }

            return ValidationProblem();
        }

        // DELETE: api/Book/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _repository.Delete(id);
            return Ok();
        }
    }
}
