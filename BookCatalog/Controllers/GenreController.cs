﻿using System.Linq;

namespace BookCatalog.Controllers
{
    using System.Collections.Generic;
    using Microsoft.AspNetCore.Mvc;
    using Models;
    using Models.Repository;

    [Route("api/[controller]")]
    [ApiController]
    public class GenreController : ControllerBase
    {
        private readonly IRepository<Genre> _repository;
        public GenreController(IRepository<Genre> repository)
        {
            this._repository = repository;
        }

        // GET: api/Genre
        [HttpGet]
        public IEnumerable<Genre> Get()
        {
            var genres = new List<Genre>(_repository.Get());
            return genres;
        }

        // GET: api/Genre/5
        [HttpGet("{id}", Name = "GetGenreById")]
        public Genre Get(int id)
        {
            Genre genre = _repository.Get(id);
            return genre;
        }

        // POST: api/Genre
        [HttpPost]
        public void Post([FromBody] Genre value)
        {
            _repository.Add(value);
        }

        // PUT: api/Genre/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] Genre value)
        {
            _repository.Update(id, value);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _repository.Delete(id);
        }
    }
}
