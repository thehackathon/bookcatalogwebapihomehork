﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using BookCatalog.Models;
using BookCatalog.Models.Repository;

namespace BookCatalog.Controllers
{
    public class HomeController : Controller
    {
        public HomeController()
        {
            
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult EditBook([FromServices]IRepository<Genre> genreRepository, [FromServices]IBookRepository bookRepository, int id)
        {
            ViewBag.Genres = genreRepository.Get();
            var book = bookRepository.Get(id);
            ViewBag.Book = book;
            ViewBag.DateFormat = book.PublishDate.ToString("yyyy-MM-dd");

            return View();
        }

        [HttpGet]
        public IActionResult CreateBook([FromServices]IRepository<Genre> genreRepository)
        {
            ViewBag.Genres = genreRepository.Get();
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
