﻿namespace BookCatalog.Models.Repository
{
    using System;
    using System.Collections.Generic;
    public class LocalRepository
    {
        private static LocalRepository _instanse;

        private LocalRepository()
        {
            Genres = new List<Genre>();
            Books = new List<Book>();

            Init();
            // DEBUG: initial values
        }

        public ICollection<Genre> Genres { get; set; }
        public ICollection<Book> Books { get; set; }

        public static LocalRepository Get => _instanse ??= new LocalRepository();

        private void Init()
        {
            var comicGenre = new Genre {Name = "Comics & Graphic Novels"};
            var fantasyGenre = new Genre {Name = "Science Fiction & Fantasy"};
            var romanceGenre = new Genre {Name = "Romance"};
            var scienceGenre = new Genre {Name = "Science & Math"};
            Genres.Add(comicGenre);
            Genres.Add(fantasyGenre);
            Genres.Add(romanceGenre);
            Genres.Add(scienceGenre);

            // comic
            var strangePlanetBook = new Book
            {
                Title = "Strange Planet", 
                Author = "Nathan W. Pyle",
                Publisher = "Morrow Gift", 
                PublishDate = new DateTime(2019, 11, 19),
                GenreId = comicGenre.Id,
            };
            var loserthinkBook = new Book
            {
                Title = "Loserthink: How Untrained Brains Are Ruining America",
                Author = "Scott Adams",
                Publisher = "Portfolio",
                PublishDate = new DateTime(2019, 11, 5),
                GenreId = comicGenre.Id,
            };
            Books.Add(strangePlanetBook);
            Books.Add(loserthinkBook);

            // fantasy
            var voloBook = new Book
            {
                Title = "Volo's Guide to Monsters (Dungeons & Dragons)",
                Author = "Wizards RPG Team",
                Publisher = "Wizards of the Coast; Wizards RPG Team edition",
                PublishDate = new DateTime(2016, 11, 15),
                GenreId = fantasyGenre.Id,
            };
            Books.Add(voloBook);

            //  science
            var noOneIsTooSmallBook = new Book
            {
                Title = "No One Is Too Small to Make a Difference",
                Author = "Greta Thunberg",
                Publisher = "Penguin Books",
                PublishDate = new DateTime(2019, 11, 12),
                GenreId = scienceGenre.Id,
            };
            Books.Add(noOneIsTooSmallBook);
        }
    }
}