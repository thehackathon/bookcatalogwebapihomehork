﻿namespace BookCatalog.Models.Repository
{
    using System.Collections.Generic;
    using System.Linq;
    public class LocalGenreRepository : IRepository<Genre>
    {
        private int _genreId;
        public LocalGenreRepository()
        {
            _genreId = LocalRepository.Get.Genres.Max(g => g.Id);
        }
        public IEnumerable<Genre> Get()
        {
            return LocalRepository.Get.Genres;
        }

        public Genre Get(int id)
        {
            return LocalRepository.Get.Genres.FirstOrDefault(g => g.Id.Equals(id));
        }

        public int Add(Genre value)
        {
            value.Id = ++_genreId;
            LocalRepository.Get.Genres.Add(value);
            return value.Id;
        }

        public void Update(int id, Genre value)
        {
            var genre = Get(id);
            if (genre != null)
            {
                genre.Name = value.Name;
            }
        }

        public void Delete(int id)
        {
            var genre = Get(id);
            if (genre != null)
            {
                LocalRepository.Get.Genres.Remove(genre);
            }
        }
    }
}
