﻿namespace BookCatalog.Models.Repository
{
    using System.Collections.Generic;
    public interface IBookRepository : IRepository<Book>
    {
        IEnumerable<Book> GetByGenre(int genreId);
    }
}
