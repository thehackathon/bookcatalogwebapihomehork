﻿namespace BookCatalog.Models.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    public class LocalBookRepository : IBookRepository
    {
        private static int _bookId;
        static LocalBookRepository()
        {
            _bookId = LocalRepository.Get.Books.Max(b =>
            {
                if (LocalRepository.Get.Books.Count > 0)
                {
                    return b.Id;
                }
                return 0;
            });
        }
        
        public IEnumerable<Book> Get()
        {
            return LocalRepository.Get.Books;
        }

        public IEnumerable<Book> GetByGenre(int genreId)
        {
            return LocalRepository.Get.Books.Where(b => b.GenreId.Equals(genreId));
        }
        
        public Book Get(int id)
        {
            return Get().FirstOrDefault(b => b.Id.Equals(id));
        }
        
        public int Add(Book value)
        {
            var genre = LocalRepository.Get.Genres.SingleOrDefault(g => g.Id.Equals(value.GenreId));
            if (genre != null)
            {
                value.Id = ++_bookId;
                LocalRepository.Get.Books.Add(value);
                return value.Id;
            }

            return -1;
        }

        public void Update(int id, Book value)
        {
            var book = Get(id);
            if (book != null)
            {
                if (!string.IsNullOrWhiteSpace(value.Author) && !value.Author.Equals(book.Author))
                {
                    book.Author = value.Author;
                }

                if (!string.IsNullOrWhiteSpace(value.Publisher) && !value.Publisher.Equals(book.Publisher))
                {
                    book.Publisher = value.Publisher;
                }

                if (!string.IsNullOrWhiteSpace(value.Title) && !value.Title.Equals(book.Title))
                {
                    book.Title = value.Title;
                }

                if (value.PublishDate != DateTime.MinValue && !value.PublishDate.Equals(book.PublishDate))
                {
                    book.PublishDate = value.PublishDate;
                }
            }
        }

        public void Delete(int id)
        {
            var book = Get(id);
            if (book != null)
            {
                LocalRepository.Get.Books.Remove(book);
            }
        }
    }
}
