﻿namespace BookCatalog.Models.Repository
{
    using System.Collections.Generic;
    public interface IRepository<T>
    {
        IEnumerable<T> Get();
        T Get(int id);
        int Add(T value);
        void Update(int id, T value);
        void Delete(int id);
    }
}
