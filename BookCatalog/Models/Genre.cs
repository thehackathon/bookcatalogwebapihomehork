﻿using System.ComponentModel.DataAnnotations;

namespace BookCatalog.Models
{
    using System.Collections.Generic;
    public class Genre
    {
        private static int _sId = 0;
        [Key]
        [ScaffoldColumn(false)]
        public int Id { get; set; } = ++_sId;
        [Required]
        public string Name { get; set; }
        //[ScaffoldColumn(false)]
        //public ICollection<Book> Books { get; set; } = new List<Book>();
    }
}