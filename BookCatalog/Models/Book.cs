﻿using System.ComponentModel.DataAnnotations;

namespace BookCatalog.Models
{
    using System;
    public class Book
    {
        private static int _sId = 0;
        [Key]
        [ScaffoldColumn(false)]
        public int Id { get; set; } = ++_sId;
        [Required]
        public string Title { get; set; }
        [Required]
        public string Author { get; set; }
        [Required]
        public string Publisher { get; set; }
        [Required]
        [DataType(DataType.Date)]
        public DateTime PublishDate { get; set; }
        [Required]
        public int GenreId { get; set; }
    }
}
